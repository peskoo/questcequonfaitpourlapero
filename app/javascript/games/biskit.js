const btn = document.querySelector('.btn-roll p');
const dice1 = document.querySelector('.dice1');
const dice2 = document.querySelector('.dice2');

if (btn) {
    btn.addEventListener('click', function(e) {
        const dice = [1, 2, 3, 4, 5, 6];
        const number1 = [Math.floor(Math.random() * (dice.length + 1))];
        const number2 = [Math.floor(Math.random() * (dice.length + 1))];

        // afficher la face du dé 1
        diceOne(number1);

        // afficher la face du dé 2
        diceTwo(number2);
        
        // loader 
        let setTimer = 10;
        setInterval(function() {
            const action = document.querySelector("#action p");
            const countDown = setTimer--;
            const counter = `
                <p>${countDown}</p>
            `;
            if (countDown >= 0){
                action.innerHTML = counter
            };
        }, 1000);
    });
};

function diceOne(number1){
    const cube1 = document.getElementById('cube1')
    if (number1[0] == 1) {
        cube1.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube1.classList.add('show-front');
    } else if (number1[0] == 2) {
        cube1.classList.remove('show-front', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube1.classList.add('show-back');
    } else if (number1[0] == 3) {
        cube1.classList.remove('show-back', 'show-front', 'show-left', 'show-top', 'show-bottom');
        cube1.classList.add('show-right');
    } else if (number1[0] == 4) {
        cube1.classList.remove('show-back', 'show-right', 'show-front', 'show-top', 'show-bottom');
        cube1.classList.add('show-left');
    } else if (number1[0] == 5) {
        cube1.classList.remove('show-back', 'show-right', 'show-left', 'show-front', 'show-bottom');
        cube1.classList.add('show-top');
    } else if (number1[0] == 6) {
        cube1.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-front');
        cube1.classList.add('show-bottom');
    } else {
        cube1.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube1.classList.add('show-front');
    }
};

function diceTwo(number2) {
    const cube2 = document.getElementById('cube2')
    if (number2[0] == 1) {
        cube2.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube2.classList.add('show-front');
    } else if (number2[0] == 2) {
        cube2.classList.remove('show-front', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube2.classList.add('show-back');
    } else if (number2[0] == 3) {
        cube2.classList.remove('show-back', 'show-front', 'show-left', 'show-top', 'show-bottom');
        cube2.classList.add('show-right');
    } else if (number2[0] == 4) {
        cube2.classList.remove('show-back', 'show-right', 'show-front', 'show-top', 'show-bottom');
        cube2.classList.add('show-left');
    } else if (number2[0] == 5) {
        cube2.classList.remove('show-back', 'show-right', 'show-left', 'show-front', 'show-bottom');
        cube2.classList.add('show-top');
    } else if (number2[0] == 6) {
        cube2.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-front');
        cube2.classList.add('show-bottom');
    } else {
        cube2.classList.remove('show-back', 'show-right', 'show-left', 'show-top', 'show-bottom');
        cube2.classList.add('show-front');
    }
};


// Affichez les règles

const btnRules = document.querySelector('.rules-btn');
const rules = document.querySelector('.rules-list');
const textUp = document.querySelector('.rules-btn p');
const textDown = document.querySelector('.rules-btn span');

if (btnRules) {
    btnRules.addEventListener('click', function(e) {
        rules.classList.toggle('hidden')
        textUp.classList.toggle('hidden')
        textDown.classList.toggle('hidden')
    });
};