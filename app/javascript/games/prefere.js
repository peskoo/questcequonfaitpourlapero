const wrapperPreferes = document.querySelector('.wrapper-preferes')

if (wrapperPreferes){
    // 1st question
    const card = document.querySelector('.question1')

    const question = document.querySelector('#vote1')
    const result = document.querySelector('.resultat1')

    card.addEventListener('click', (e)=> {
        e.preventDefault
        if (!question2.classList.contains('hidden')){
            question.classList.add('hidden')
            result.classList.remove('hidden')
        };
    })


    // 2nd question

    const card2 = document.querySelector('.question2')

    const question2 = document.querySelector('#vote2')
    const result2 = document.querySelector('.resultat2')

    card2.addEventListener('click', (e)=> {
        if (!question.classList.contains('hidden')) {
            question2.classList.add('hidden')
            result2.classList.remove('hidden')
        }
    })

    // Changement de couleurs du background reponse en fonction
    // du pourcentage de réponse

    const pourcent = Array.from(document.querySelectorAll('.question_pourcent h2'))
    let pourcentArray = [];
    
    pourcent.forEach(function(number){
        pourcentArray.push(number.textContent)
    })
    
    const pourcent_1_array = pourcentArray[0].split(' ')
    const pourcent_2_array = pourcentArray[1].split(' ')
    const pourcent1 = pourcent_1_array[0]
    const pourcent2 = pourcent_2_array[0]

    console.log(pourcent1)
    console.log(pourcent2)
    
    if (pourcent1 == pourcent2) {
        result.classList.remove('red')
        result2.classList.remove('red')
        result.classList.add('green')
        result2.classList.add('green')
    } else if (pourcent1 == 50 || pourcent2 == 50) {
        result.classList.remove('red')
        result2.classList.remove('red')
        result.classList.add('green')
        result2.classList.add('green')
    } else if (pourcent2 > pourcent1) {
        result2.classList.remove('red')
        result2.classList.add('green')
    }  else if (pourcent1 > pourcent2) {
        result.classList.remove('red')
        result.classList.add('green')
    } 
}

