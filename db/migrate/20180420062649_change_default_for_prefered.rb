class ChangeDefaultForPrefered < ActiveRecord::Migration[5.1]
  def change
    change_column :prefereds, :vote1, :integer, :default => 0
    change_column :prefereds, :vote2, :integer, :default => 0
  end
end
