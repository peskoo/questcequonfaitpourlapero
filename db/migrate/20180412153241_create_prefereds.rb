class CreatePrefereds < ActiveRecord::Migration[5.1]
  def change
    create_table :prefereds do |t|
      t.text :text
      t.timestamps
    end
  end
end
