class ChangeTypeOfTextFromPrefered < ActiveRecord::Migration[5.1]
  def change
    change_column :prefereds, :text, :string
  end
end
