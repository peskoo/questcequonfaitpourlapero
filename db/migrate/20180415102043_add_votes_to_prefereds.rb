class AddVotesToPrefereds < ActiveRecord::Migration[5.1]
  def change
    add_column :prefereds, :vote1, :integer
    add_column :prefereds, :vote2, :integer
    add_column :prefereds, :text2, :string
  end
end
