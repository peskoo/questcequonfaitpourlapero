Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#index'
  get 'biskit', to: 'biskits#biskit'
  get 'jenaijamais', to: 'nevers#jenaijamais'

  resources :prefereds do
    member do
      get :update_vote2
    end
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :nevers, only: [:index]
      resources :prefereds, only: [:index]
    end
  end
end
